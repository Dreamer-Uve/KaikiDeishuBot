import { Command } from "@cataclym/discord-akairo";
import { Message, MessageEmbed } from "discord.js";
import { Image } from "kaori/typings/Image";
import { grabHentaiPictureAsync, grabHentai, typesArray, postHentai } from "./hentaiService";

export default class HentaiCommand extends Command {
	constructor() {
		super("hentai", {
			aliases: ["hentai"],
			description: { description: "Fetches hentai images from Booru boards" },
			typing: true,
			args: [{
				id: "tags",
				match: "rest",
				type: "string",
				default: null,
			}],
		});
	}
	public async exec(message: Message, { tags }: { tags: string }): Promise<Message> {

		if (!tags) {
			return message.channel.send(await grabHentai(typesArray[Math.floor(Math.random() * typesArray.length)], "single"));
		}

		const messageArguments: string[] | undefined = tags?.split(/ +/);
		return postHentai(message, messageArguments);
	}
}
