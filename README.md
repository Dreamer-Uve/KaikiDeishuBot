#  Welcome to Kaiki Deishu Bot

### A simple bot Written in TypeScript with lots of unique commands and features.

Invite the public bot here: TODO: Add link

***
## Requirements
* Nodejs https://nodejs.org/en/ (14.16.0 for best compatibility) 
* TypeScript `sudo npm install -g typescript` https://www.npmjs.com/package/typescript
* node-canvas: This is only necessary if your npm install tries to compile node-canvas from source. 
    * (Windows) See the <a href="https://github.com/Automattic/node-canvas/wiki/Installation:-Windows">wiki</a> 
    * (Linux)
        * Debian/Ubuntu: `sudo apt-get install build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev` 
        * RPM/Fedora: `sudo yum install gcc-c++ cairo-devel libjpeg-turbo-devel pango-devel giflib-devel`
        * Solus: `sudo eopkg it libjpeg-turbo6 libjpeg-turbo-devel`
        * Other: See the <a href="https://github.com/Automattic/node-canvas/wiki">wiki</a>
    * (MacOS)
        * Homebrew: `brew install pkg-config cairo pango libpng jpeg giflib librsvg`

## Instructions

  1. Clone repo: `git clone https://gitlab.com/cataclym/KaikiDeishuBot.git`
  1. `cd KaikiDeishuBot`
  1. Create .env file: `cp .env.example .env`.
  1. Edit .env to add your token and owner id. Using nano or your editor of choice: `nano .env`
  1. `npm i`
  1. `npm start`
  1. ?
  1. Profit

Having issues? Come join us on discord                                                                                  

<a href="https://discord.gg/msNtTYV">
<img src="https://discordapp.com/api/guilds/414099963841216512/embed.png?style=banner2" title="Discord Server">
</a>

***
                                                         
You can use the following link to invite your bot: Replace `YOUR_BOT_ID_HERE` with your bot id/client id.
https://discord.com/oauth2/authorize?client_id=YOUR_BOT_ID_HERE&scope=bot

Feel like contributing? Please help me! :D I have not written any guides, but I'll review all MRs.

# Contributing
  1. Fork the bot!
  1. Create your feature branch: `git checkout -b cool-new-branch`
  1. Commit your changes: `git commit -am "Added x feature!"`
  1. Push to the branch: `git push origin My-new-feature`
  1. Submit a pull request!

# I owe some amazing people thanks, and more!
- Huge thanks to @Arvfitii for helping me whenever im in need!
- Thanks to @rjt-rockx on Discord for so much help and time!
- Should also mention @shivaco for help <3
- Thanks also to @Kwoth <3

# About
### A bit about this project
This bot was my first entry into javascript, nodejs, discordjs and programming/coding overall. It has been very fun and also frustrating. If you do find unoptimized code and or flaws or other inherently bad code - I would be very happy to merge changes and try to learn from my own mistakes. Commenting the code helps! I will try to do so as well.

Update: Learning TypeScript has been a very good experience.